# Ansible

##ping
ansible all -i hosts.yml --user={{user}} -p "ping"

##plabook exec
ansible-playbook -i hosts.yml --user={{user}} playbook.yml





**Ansible is Simple IT Automation**


Doc : Ansible Documentation — Ansible Documentation


Install : https://docs.ansible.com/ansible/latest/installation_guide/


|OS	| Install command |
|----|----|
|Redhat or CentOS |	sudo yum install ansible  |
|Fedora	| sudo dnf install ansible |
|Ubuntu |	sudo apt get install ansible |
|PIP	| sudo pip install ansible |




| Install via PIP | Command |
|----|----|
| Install pip if not present | $ sudo yum install epel release  $ sudo yum install python pip |
| Install Ansible using pip | $ sudo pip install ansible |
| Upgrade Ansible using pip | $ sudo pip install upgrade ansible |
| Install Specific Version of Ansible using pip | $ sudo pip install ansible 2.4 |
